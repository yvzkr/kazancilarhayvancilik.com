class Admin::PageController < AdminController
  before_action :set_page, only: [:edit, :update, :destroy]
  def index
    @pages = Page.all
  end

  def new
    @categories = Category.all
  end

  def create
    @page = Page.new(post_params)
    respond_to do |format|
      if @page.save
        format.html {redirect_to admin_page_index_path, notice: "Kaydınız basarili" }
      else
        @categories = Category.all
        format.html { render :new }
      end
    end
  end

  def edit
    @categories = Category.all
  end

  def update
    if @page.update(params[:post].permit(:title, :content, :category_id))
      redirect_to admin_page_index_path
    else
      @categories = Category.all
      render 'edit'
    end
  end

  def destroy
    @page.destroy
    redirect_to admin_page_index_path
  end

  private
  def post_params
    params.require(:post).permit(:title, :content, :category_id)
  end

  def set_page
    @page = Page.find(params[:id])
  end





end
