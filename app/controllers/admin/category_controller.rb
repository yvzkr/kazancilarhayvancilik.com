class Admin::CategoryController < AdminController
  before_action :authenticate_user!
  before_action :set_admin_category, only: [:destroy, :edit, :update]
  #get admin/category
  def index
    @categories = Category.all
  end
  # GET /admin/category/1
  def new
  end
  # POST /admin/categoru
  def create
    @category = Category.new(post_params)
    respond_to do |format|
      if @category.save
        format.html {redirect_to admin_category_index_path, notice: "Kaydınız basarili" }
      else
        format.html { render :new }
      end
    end
  end

  def destroy
    #@category = Category.find(params[:id])
    @category.destroy
    redirect_to admin_category_index_path
  end

  def edit
  end

  def update
    if @category.update(params[:post].permit(:title, :description, :isVisible))
      redirect_to admin_category_index_path
    else
      render 'edit'
    end
  end

  private
  def post_params
    params.require(:post).permit(:title, :description, :isVisible)
  end

  def set_admin_category
    @category = Category.find(params[:id])
  end

end
