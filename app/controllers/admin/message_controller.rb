class Admin::MessageController < AdminController
  before_action :authenticate_user!
  def index
    @messages = Message.all.order("created_at DESC")
  end

  def show

  end
end
