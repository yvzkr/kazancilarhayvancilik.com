Rails.application.routes.draw do

  scope "/admin" do
    devise_for :users
  end
  #devise_for :users

  get 'homes/index'
  root to: "homes#index"

  namespace :admin do
    root to: "message#index"
    #mesajlar
    resources :message, only: [:show, :index]
    #kategoriler
    resources :category, except: [:show]
    #sayfalar
    resources :page, except: [:show]
    #haberler
    resources :news
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
