class AddIsVisibleToCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :isVisible, :boolean
  end
end
